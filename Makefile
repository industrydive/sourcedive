COMPOSE_FILE = docker-compose.yml
APP_CONTAINER = sourcedive_app_1

up:
	docker-compose up -d --build
	@echo "Source Dive started"

migrate:
	docker exec ${APP_CONTAINER} python manage.py migrate --noinput
	@echo "Migrations have been ran"

stop:
	docker-compose stop
	@echo "Source Dive stopped"