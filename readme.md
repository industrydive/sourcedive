This is an open-source tool for creating a database of sources. 

# Developer Setup

The following instructions are intended for a developer to set this up locally

## Preparation

Make sure you have `docker` and `docker-compose` installed

## Setup the app

Clone the repo (or your fork)

	git clone git@gitlab.com:industrydive/sourcedive.git

### Private settings

Update all the `UPDATE` vars in .env.dev as needed.

## Build & start the image

    make
    
## Run the migrations
    
    make migrate
    
## Create a superuser

    docker exec -it sourcedive_app_1 sh
    python manage.py createsuperuser # follow the command prompts
    
## Login into the app

    Go to http://127.0.0.1:8080/admin/login/?next=/admin/
    Select 'Enter credentials' and use your superuser from the previous step

# Set up GitHub

Generate ssh key 

	ssh-keygen -t rsa -b 4096 -C "youremail"

## Deploy steps

In the virtualenv:

1. `git checkout main`

2. `git pull origin main`

3. `pip3 install -r requirements.txt`

4. `python3 manage.py migrate`

5. `sudo systemctl restart uwsgi`
