import pytest

from sources.name_analysis import GetNames
from sources.models import Name


pytestmark = pytest.mark.django_db


@pytest.fixture
def test_newspost_data():
    return [
        {
            'url': 'test_url_1',
            'body': 'Hello world my name is President Bob Smith.'
        },
        {
            'url': 'test_url_2',
            'body': 'Hello world Director Sam Brown is my name.'
        },
        {
            'url': 'test_url_3',
            'body': 'CEO Lucy Bell is my name hello world.'
        }
    ]


def test_get_names(client, test_newspost_data):
    """
    Test that names are retrieved from the newspost bodies and contain expected data
    """
    for newspost in test_newspost_data:
        GetNames(newspost)

    expected_data = [
        {
            'name': 'Bob Smith',
            'title': 'President',
            'gender_guess': 'male'
        },
        {
            'name': 'Sam Brown',
            'title': 'Director',
            'gender_guess': 'mostly_male'
        },
        {
            'name': 'Lucy Bell',
            'title': 'CEO',
            'gender_guess': 'female'
        },
    ]

    names = Name.objects.all()
    for index, name in enumerate(names):
        assert name.name == expected_data[index]['name']
        assert name.title == expected_data[index]['title']
        assert name.gender_guess == expected_data[index]['gender_guess']


def test_get_names_rerun(client, test_newspost_data):
    """
    Test that when an article is re-analyzed, a name's mentions isn't counted twice.
    """
    for newspost in test_newspost_data:
        GetNames(newspost)
        GetNames(newspost)  # re-run GetNames() on the newspost

    names = Name.objects.all()
    for name in names:
        for mention_amount in name.article_mentions.values():
            assert mention_amount == 1  # mention amount should still be 1


def test_multiple_mentions(client):
    """
    Test that when a name is mentioned more than once the mentions amount is accurate
    """
    multiple_mentions_data = {
        'url': 'test_url_1',
        'body': 'Hello world my name is President Bob Smith. People call me Mr. Smith.'
    }
    GetNames(multiple_mentions_data)

    name = Name.objects.first()
    assert name.article_mentions['test_url_1'] == 2
