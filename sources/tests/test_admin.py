import pytest


pytestmark = pytest.mark.django_db


def test_email_address_semiprivate_display(person, person_admin):
    semiprivate_display = person_admin.email_address_semiprivate_display(person)
    assert semiprivate_display == f'Please contact <strong>{person.created_by}</strong> for this information'


def test_phone_number_primary_semiprivate_display(person, person_admin):
    semiprivate_display = person_admin.phone_number_primary_semiprivate_display(person)
    assert semiprivate_display == f'Please contact <strong>{person.created_by}</strong> for this information'


def test_phone_number_secondary_semiprivate_display(person, person_admin):
    semiprivate_display = person_admin.phone_number_secondary_semiprivate_display(person)
    assert semiprivate_display == f'Please contact <strong>{person.created_by}</strong> for this information'


def test_get_expertise_with_person_that_has_expertise(person_admin, person, expertise):
    person.expertise.add(*expertise)
    expertise_string = person_admin.get_expertise(person)
    assert expertise_string == ', '.join([e.name for e in expertise])


def test_get_expertise_with_person_that_has_one_expertise(person_admin, person, expertise):
    expertise = expertise[0]
    person.expertise.add(expertise)
    expertise_string = person_admin.get_expertise(person)
    assert expertise_string == expertise.name


def test_get_expertise_with_person_that_has_no_expertise(person_admin, person):
    organization_string = person_admin.get_expertise(person)
    assert organization_string == '-'


def test_get_organizations_with_person_that_has_organizations(person_admin, person, organizations):
    person.organization.add(*organizations)
    organization_string = person_admin.get_organizations(person)
    assert organization_string == ', '.join([o.name for o in organizations])


def test_get_organizations_with_person_that_has_one_organization(person_admin, person, organizations):
    organization = organizations[0]
    person.organization.add(organization)
    organization_string = person_admin.get_organizations(person)
    assert organization_string == organization.name


def test_get_organizations_with_person_that_has_no_organizations(person_admin, person):
    organization_string = person_admin.get_organizations(person)
    assert organization_string == '-'


def test_get_correct_contact_field_names(person_admin):
    public_contact_fields = [
        'email_address',
        'phone_number_primary',
        'phone_number_secondary',
    ]

    semi_private_contact_fields = [f'{field}_semiprivate_display' for field in public_contact_fields]

    actual_contact_fields = person_admin._get_correct_contact_field_names()
    assert actual_contact_fields == public_contact_fields

    actual_contact_fields = person_admin._get_correct_contact_field_names(hide_contact_data=False)
    assert actual_contact_fields == public_contact_fields

    actual_contact_fields = person_admin._get_correct_contact_field_names(hide_contact_data=True)
    assert actual_contact_fields == semi_private_contact_fields
