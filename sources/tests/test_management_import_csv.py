import pytest
from pytest_django.asserts import assertQuerysetEqual

from sources.models import Person
from sources.management.commands.import_csv import (
    create_person,
    csv_name_to_model_name,
    _add_m2m_fields_to_source,
    _add_values_to_source,
    _create_dictionary_for_email_data,
    _get_or_create_object,
)


pytestmark = pytest.mark.django_db


@pytest.fixture
def source_data_as_dict():
    return {
        'privacy_level': 'public',
        'name': 'Testfirst Testlast20200612g',
        'type_of_expert': 'Web Creator',
        'title': 'Software Engineer II',
        'city': 'Washington',
        'state': 'DC',
        'province': '',
        'country': 'USA',
        'phone_number_primary': '202-867-5309',
        'phone_number_secondary': '202-555-4444',
        'twitter': 'industrydive',
        'import_notes': "I've talked to this source before. They're awesome!",
        'website': 'https://industrydive.com',
        'prefix': 'Mrs.',
        'entry_method': 'import',
        'entry_type': 'automated',
        'email_address': 'test20200612g@industrydive.com',
        'timezone': None,
        'gatekeeper': 'True',
        'updated': '2020-06-19 12:03:00',
    }


@pytest.fixture
def source_m2m_data_as_dict():
    # There's intentional extra spaces to make sure the methods are stripping the white space as expected
    return {
        'created_by': 'testpersontest@industrydive.com',
        'expertise': 'Big data, Flask, data journalism',
        'industries': 'Software, Journalism',
        'organization': 'Industry Fly',
        'exportable_by_dive': 'Retail, HR',
        'exportable_by_user': 'test1, test2',
    }


def test_add_values_to_source(person):
    # Check that the person doesn't already have expertise
    assertQuerysetEqual(person.expertise.all(), [], ordered=False)

    expertise = [
        {'name': 'expert tester'},
        {'name': 'lack luster developer'},
    ]

    _add_values_to_source(
        field='expertise',
        values_list=expertise,
        person_obj=person,
        app_label='sources',
        model_name='Expertise'
    )

    expected_list = [x['name'] for x in expertise]
    assertQuerysetEqual(person.expertise.all(), expected_list, transform=lambda x: x.name, ordered=False)


def test_create_dictionary_for_email_data():
    expected_email_as_dict = [
        {
            'email': 'diver@industrydive.com',
            'username': 'diver',
        },
        {
            'email': 'test@industrydive.com',
            'username': 'test',
        }
    ]

    emails_as_list = ','.join([x['username'] for x in expected_email_as_dict])
    actual_email_as_dict = _create_dictionary_for_email_data(emails_as_list)

    assert actual_email_as_dict == expected_email_as_dict


def test_get_or_create_object():
    obj_values = {
        'email': 'test@industrydive.com',
    }
    obj, obj_created = _get_or_create_object(obj_values, 'auth', 'User')

    assert obj_created is True
    assert obj.email == obj_values['email']


def test_add_m2m_fields_to_source(person, source_m2m_data_as_dict):
    def _email_lambda(x):
        return (x.email, x.username)

    def _m2m_lambda(x):
        return x.name

    _add_m2m_fields_to_source(person_obj=person, m2m_dict=source_m2m_data_as_dict)

    # For created_by, we need to set the email and username
    assert person.created_by.username == source_m2m_data_as_dict['created_by'].split('@')[0]
    assert person.created_by.email == source_m2m_data_as_dict['created_by']

    for field_key in csv_name_to_model_name:
        model_name, obj_field_name = csv_name_to_model_name[field_key]

        # For exportable_by_user, we need to set the email and username, so I want to compare those values against the queryset.
        # I'm comparing tuples of (email, username).
        # This contrast the other m2m fields because they just set the name, so only need to check the name.
        if field_key == 'exportable_by_user':
            lambda_transform = _email_lambda
            expected_list = []
            for username in source_m2m_data_as_dict[field_key].split(','):
                username = username.strip()
                expected_list.append(
                    (f'{username}@industrydive.com', username),
                )
        else:
            lambda_transform = _m2m_lambda
            expected_list = [x.strip() for x in source_m2m_data_as_dict[field_key].split(',')]

        assertQuerysetEqual(getattr(person, obj_field_name).all(), expected_list, transform=lambda_transform, ordered=False)


def test_create_person_with_new_source(source_data_as_dict, source_m2m_data_as_dict):
    create_person(data_dict=source_data_as_dict, m2m_dict=source_m2m_data_as_dict)

    actual_person = Person.objects.get(email_address=source_data_as_dict['email_address'])

    # We include the updated column in the csv, but the updated field has auto_now set to True, so the field is always
    # updated field is always set to where the now time is during import. Therefore, I'm excluding from validation
    source_data_as_dict.pop('updated')

    for field_key in source_data_as_dict:
        actual_value = getattr(actual_person, field_key)
        expected_value = source_data_as_dict[field_key]

        if field_key == 'gatekeeper':
            expected_value = bool(expected_value)

        assert actual_value == expected_value


def test_create_person_with_existing_source(person, user, source_data_as_dict, source_m2m_data_as_dict):
    # Currently, we are only adding new sources on import and not updating existing sources
    def _test_fields_are_blank(person):
        assert person.created_by == user

        for field_key in csv_name_to_model_name:
            model_name, obj_field_name = csv_name_to_model_name[field_key]
            assertQuerysetEqual(getattr(person, obj_field_name).all(), [], ordered=False)

    # Before calling the method, all the fields should be blank
    _test_fields_are_blank(person)

    source_data_as_dict['email_address'] = person.email_address
    create_person(data_dict=source_data_as_dict, m2m_dict=source_m2m_data_as_dict)

    # After calling the method, all the fields should be blank
    _test_fields_are_blank(person)
