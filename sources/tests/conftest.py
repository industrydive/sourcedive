from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import User

import pytest

from sources.admin import PersonAdmin
from sources.models import Expertise, Organization, Person


@pytest.fixture
def user():
    user = User(username='test', email='test@test.com', password='test')
    user.save()
    return user


@pytest.fixture
def person_admin():
    return PersonAdmin(Person, AdminSite())


@pytest.fixture
def person(user):
    person = Person(name='test', email_address='test@test.com', privacy_level='test', created_by=user)
    person.save()
    return person


@pytest.fixture
def expertise():
    temp = []
    for expertise in ['Cool', 'Expert', 'Clouds']:
        e = Expertise(name=expertise)
        e.save()
        temp.append(e)

    return temp


@pytest.fixture
def organizations():
    temp = []
    for org_name in ['Cool', 'Expert', 'Clouds']:
        o = Organization(name=org_name)
        o.save()
        temp.append(o)

    return temp
