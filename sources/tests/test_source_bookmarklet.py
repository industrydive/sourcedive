from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission, User
from django.urls import reverse

import pytest
from pytest_django.asserts import assertTemplateUsed, assertContains, assertRedirects

from sources.models import Person

pytestmark = pytest.mark.django_db


# If not staff then the client doesn't have permission to go to the /admin page.
@pytest.fixture
def user_no_privilege():
    user = User(username='test', email='test@test.com', password='test', is_staff=True)
    user.save()
    return user


@pytest.fixture
def user_with_privilege():
    user = User(username='test', email='test@test.com', password='test', is_staff=True)
    user.save()

    ct = ContentType.objects.get(app_label='sources', model='person')
    p, _ = Permission.objects.get_or_create(content_type=ct, codename='add_person')
    user.user_permissions.add(p)

    return user


@pytest.fixture
def form_data():
    return {
        'privacy_level': 'public',
        'name': '',
        'email_address': 'test@test.com',
        'phone_number_primary': '',
        'title': '',
        'created_by': '',
    }


def test_bookmarklet_setup_link(client, user_no_privilege):
    """ Test that the setup link appears on the admin index page """
    client.force_login(user_no_privilege)
    response = client.get(reverse('admin:index'))

    bookmarket_url = reverse('bookmarklet_setup')
    expected_link = f'<a href="{bookmarket_url}">Setup Source bookmarklet</a>'

    assert response.status_code == 200
    assertContains(response, expected_link)


def test_bookmarklet_form_with_privilege(client, user_with_privilege):
    """ Test user with the correct permission can access the form """
    client.force_login(user_with_privilege)
    response = client.get(reverse('bookmarklet'))

    assert response.status_code == 200
    assertTemplateUsed(response, 'admin/sources/bookmarklet/form.html')


def test_bookmarklet_form_with_no_privilege(client, user_no_privilege):
    """ Test user without the correct permission cannot access the form """
    client.force_login(user_no_privilege)
    response = client.get(reverse('bookmarklet'))

    assertRedirects(response, f"{reverse('admin:index')}?next={reverse('bookmarklet')}")


def test_bookmarklet_form_add_with_correct_data(client, user_with_privilege, form_data):
    """ Test user with the correct permission can access the form """
    name = 'testor correcty datason'
    form_data['name'] = name
    form_data['created_by'] = f'{user_with_privilege.id}'

    client.force_login(user_with_privilege)
    response = client.post(reverse('bookmarklet_add'), data=form_data)

    assert response.status_code == 200
    assertContains(response, 'You may now close this window.')
    assert Person.objects.get(name=name)
