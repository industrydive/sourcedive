import json

from django import forms
from django.contrib import admin
from django.contrib.admin.filters import SimpleListFilter
from django.contrib.admin.utils import flatten_fieldsets
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User
from django.db.models import Q
from django.forms import ModelChoiceField, ModelMultipleChoiceField, Select
from django.http import HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from jsonfield.fields import JSONField

from sources.models import (
    Dive,
    Expertise,
    Industry,
    Interaction,
    Organization,
    Person,
    Name
)


class CreatedByMixin(object):

    def get_created_by(self, obj):
        """
            This will display a custom created_by field by using
            the user's first and last name instead of the user's username
        """
        return get_user_display_name(obj.created_by)
    get_created_by.short_description = 'Created By'


def get_user_display_name(obj):
    return obj.get_full_name() or obj.username


class UserChoiceField(ModelMultipleChoiceField):
    """
        This will change how the user (might be field interviewer, etc) is
        displayed inside the dropdowns. It will now show the user's full name
        instead of the user's username.
    """
    def label_from_instance(self, obj):
        return get_user_display_name(obj)


class UserSingleChoiceField(ModelChoiceField):

    def label_from_instance(self, obj):
        return get_user_display_name(obj)


class DiveAdmin(admin.ModelAdmin):
    fields = ['name', 'users']
    filter_horizontal = ['users']
    list_display = ['name']
    search_fields = ['name']

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """
            Overwrites formfield_for_manytomany is db field is users.
            Allows us to display user's full name in select dropdowns
        """
        if db_field.name == 'users':
            return UserChoiceField(queryset=User.objects.all(), widget=FilteredSelectMultiple('users', is_stacked=False))
        return super().formfield_for_manytomany(db_field, request, **kwargs)


class ExpertiseAdmin(admin.ModelAdmin):
    fields = ['name']
    list_display = ['name']
    search_fields = ['name']


class IndustryAdmin(admin.ModelAdmin):
    fields = ['name']
    list_display = ['name']
    search_fields = ['name']


# TO-DO: need a way to hide private interactions in the inline
# see https://stackoverflow.com/a/47261297
class InteractionInline(admin.TabularInline, CreatedByMixin):
    model = Interaction
    # the fields are listed explicity to avoid showing notes, which can't be easily displayed like the other hidden field values
    fields = ['privacy_level', 'date_time', 'interaction_type', 'interviewee', 'interviewers_listview', 'get_created_by', 'notes_view']

    max_num = 0
    readonly_fields = fields  # ['notes_semiprivate']
    show_change_link = True
    classes = ['interactions-previous']

    def notes_view(self, obj):
        """ Generate note text replacement depending on privacy """
        url = reverse('admin:sources_interaction_change', args=(obj.id,))
        if obj.privacy_level == 'searchable':
            display_text = 'Contact <strong>{}</strong> for these notes. <a href="{}">View interaction page</a>.'.format(
                obj.created_by,
                url
            )
        else:
            display_text = obj.notes
        return format_html(display_text)
    notes_view.short_description = 'Notes'

    def get_queryset(self, request):
        """ only show private interactions to the person who created them """
        qs = super(InteractionInline, self).get_queryset(request)

        # if the interaction is not private, then include them
        # if the interaction is private and created by that user, then include them
        return qs.filter(
            # IMPORANT! don't give superusers access to everything
            ~Q(privacy_level__contains='private') |
            Q(created_by=request.user, privacy_level='private_individual')
        )

    def interviewers_listview(self, obj):
        return InteractionAdmin.interviewers_listview(None, obj)
    interviewers_listview.short_description = 'Interviewer(s)'


class InteractionNewInline(admin.TabularInline, CreatedByMixin):
    model = Interaction
    fields = ['privacy_level', 'date_time', 'interaction_type', 'interviewee', 'interviewer', 'created_by', 'notes']
    extra = 0
    verbose_name = 'interaction (be sure to "save" after)'
    classes = ['interactions-new']

    def get_queryset(self, request):
        """ show none """
        qs = super(InteractionNewInline, self).get_queryset(request)

        return qs.none()

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
            Overwrites formfield_for_foreignkey when db field is created_by.
            Allows us to display user's full name in select dropdown. Passes in
            a class in the select widget to enforce the correct dropdown.
        """
        if db_field.name == 'created_by':
            return UserSingleChoiceField(queryset=User.objects.all(), widget=Select(attrs={'class': 'select'}), initial=request.user.id)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """
            Overwrites formfield_for_manytomany when db field is interviewer.
            Allows us to display user's full name in select dropdowns
        """
        if db_field.name == 'interviewer':
            return UserChoiceField(queryset=User.objects.all(), widget=FilteredSelectMultiple('interviewer', is_stacked=False))
        return super().formfield_for_manytomany(db_field, request, **kwargs)


class InteractionAdmin(admin.ModelAdmin, CreatedByMixin):
    list_display = ['interviewee', 'interaction_type', 'date_time', 'get_created_by', 'interviewers_listview', 'privacy_level']
    list_filter = ['interaction_type']
    filter_horizontal = ['interviewer']

    _fields_always_readonly = ['get_created_by']
    _fields_before_notes = ['privacy_level', 'date_time', 'interaction_type', 'interviewee', 'interviewer']
    _fields_after_notes = ['get_created_by']

    def _determine_whether_to_hide_notes(self, request, obj):
        """
        Given a request and an object, determine if we need to hide the contact data for the object. If
        the object is None (doesn't exist yet), always return False (we don't need to hide anything).

        Returns True if we need to hide the notes, False if the user has permissions to see/edit the notes.

        NOTE: this could be abstracted out, as it is virtually identical to _determine_whether_to_hide_contact_data
        in PersonAdmin.
        """
        # Needed so it doesn't break for /add pages
        if obj:
            privacy_level = obj.privacy_level
            current_user = request.user

            # Show note on semi-private interaction to a user who is an interviewer
            if privacy_level == 'searchable' and current_user in obj.interviewer.all():
                return False
            # Hide note if the interaction is semi-private or private and the user
            # did not create it.
            elif privacy_level in ['searchable', 'private_individual'] and obj.created_by != current_user:
                return True
        else:
            return False

    def get_readonly_fields(self, request, obj=None):
        """
        If the user doesn't have permission to see the notes, both the notes display and the
        privacy level are added to the default readonly fields.

        Use Django's built in hook for accessing readonly fields. NOTE: Manipulating self.readonly_fields
        directly leads to problems.

        Also, make all fields read-only if the current user:
            - did not create the interaction
            - was not listed as an interviewer
        """
        # Needed so it doesn't break for /add pages
        if obj:
            hide_data = self._determine_whether_to_hide_notes(request, obj)

            current_user = request.user
            created_by_someone_else = current_user != obj.created_by
            not_an_interviewer = current_user not in obj.interviewer.all()
            is_interviewer = current_user in obj.interviewer.all()

            # TODO: Use a more generic approach so nothing falls through if new
            # fields are added. They should ideally be captured by this approach
            # because new fields would need to be before or after notes.
            all_fields_except_notes = self._fields_always_readonly + self._fields_before_notes + self._fields_after_notes

            # Semi-private, created by someone else, not an interviewer
            if hide_data and created_by_someone_else and not_an_interviewer:
                return all_fields_except_notes + ['notes_semiprivate_display']
            # Semi-private, is interviewer
            elif hide_data and is_interviewer:
                return self._fields_always_readonly + ['notes_semiprivate_display', 'privacy_level']
            # Created by someone else and is interviewer
            elif created_by_someone_else and is_interviewer:
                return self._fields_always_readonly + ['interviewer', 'privacy_level']
            # Created by someone else and not an interviewer
            elif created_by_someone_else and not_an_interviewer:
                return all_fields_except_notes + ['notes']
            # Current user is creator
            elif not created_by_someone_else:
                return self._fields_always_readonly
        else:
            return self._fields_always_readonly

    def get_fields(self, request, obj=None):
        """
        If the user doesn't have permission to view the notes, display the replacement notes field instead.

        Use Django's built in hook for accessing fields. NOTE: Manipulating self.fields directly can lead to problems.
        """
        hide_data = self._determine_whether_to_hide_notes(request, obj)
        if hide_data:
            return self._fields_before_notes + ['notes_semiprivate_display'] + self._fields_after_notes
        else:
            return self._fields_before_notes + ['notes'] + self._fields_after_notes

    def get_queryset(self, request):
        """ only show private interactions to the person who created them """
        qs = super(InteractionAdmin, self).get_queryset(request)

        # if the source is not private, then include them
        # if the source is private and created by that user, then include them
        return qs.filter(
            # IMPORANT! don't give superusers access to everything
            ~Q(privacy_level__contains='private') |
            Q(created_by=request.user, privacy_level='private_individual')
        )

    def save_model(self, request, obj, form, change):
        # associate the Interaction being created with the User who created them
        current_user = request.user
        if not obj.created_by:
            obj.created_by = current_user

        # save
        super(InteractionAdmin, self).save_model(request, obj, form, change)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """
            Overwrites formfield_for_manytomany when db field is interviewer.
            Allows us to display user's full name in select dropdowns
        """
        if db_field.name == 'interviewer':
            return UserChoiceField(queryset=User.objects.all(), widget=FilteredSelectMultiple('interviewer', is_stacked=False))
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def interviewers_listview(self, obj):
        interviewers_list = obj.interviewer.all()
        interviewers = [get_user_display_name(interviewer) for interviewer in interviewers_list]
        return ', '.join(interviewers)
    interviewers_listview.short_description = 'Interviewer(s)'

    def notes_semiprivate_display(self, obj):
        display_text = 'Please contact <strong>{}</strong> for this information'.format(obj.created_by)
        return format_html(display_text)
    notes_semiprivate_display.short_description = 'Notes'


class OrganizationAdmin(admin.ModelAdmin):
    fields = ['name']
    list_display = ['name']
    search_fields = ['name']


# for SimpleListFilter classes
all_sources = Person.objects.all()
private_sources = all_sources.filter(privacy_level='private_individual')
non_private_sources = all_sources.exclude(privacy_level='private_individual')


# for SimpleListFilter classes
def get_displayable_list(private_items, non_private_items):
    overlap_set = set(private_items) & set(non_private_items)

    set(non_private_items).update(overlap_set)

    displayable_list = list(set(non_private_items))

    return displayable_list


class ExpertiseFilter(SimpleListFilter):
    title = 'Expertise'
    parameter_name = 'expertise__name'

    def lookups(self, request, model_admin):
        private_expertise = [expertise.name for source in private_sources for expertise in source.expertise.all()]
        non_private_expertise = [expertise.name for source in non_private_sources for expertise in source.expertise.all()]

        options = get_displayable_list(private_expertise, non_private_expertise)
        filters_list = [(option, option) for option in options]

        return tuple(filters_list)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(expertise__name=self.value())
        else:
            return queryset


class IndustryFilter(SimpleListFilter):
    title = 'Industry'
    parameter_name = 'industries__name'

    def lookups(self, request, model_admin):
        private_industries = [industry.name for source in private_sources for industry in source.industries.all()]
        non_private_industries = [industry.name for source in non_private_sources for industry in source.industries.all()]

        options = get_displayable_list(private_industries, non_private_industries)
        filters_list = [(option, option) for option in options]

        return tuple(filters_list)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(industries__name=self.value())
        else:
            return queryset


class OrganizationFilter(SimpleListFilter):
    title = 'Organization'
    parameter_name = 'organization__name'

    def lookups(self, request, model_admin):
        private_organizations = [organization.name for source in private_sources for organization in source.organization.all()]
        non_private_organizations = [organization.name for source in non_private_sources for organization in source.organization.all()]

        options = get_displayable_list(private_organizations, non_private_organizations)
        filters_list = [(option, option) for option in options]

        return tuple(filters_list)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(organization__name=self.value())
        else:
            return queryset


class PersonAdmin(admin.ModelAdmin, CreatedByMixin):
    list_display = ['name', 'get_organizations', 'get_created_by', 'get_expertise']
    list_filter = [IndustryFilter, ExpertiseFilter, OrganizationFilter, 'created_by', 'privacy_level', 'gatekeeper']
    search_fields = [
        'city',
        'country',
        'email_address',
        'expertise__name',
        'name',
        'import_notes',
        'organization__name',
        'state',
        'title',
        'type_of_expert',
        'twitter',
        'website',
    ]
    filter_horizontal = ['expertise', 'industries', 'organization', 'exportable_by_dive', 'exportable_by_user']
    readonly_fields = ['entry_method', 'entry_type', 'get_created_by', 'updated', 'import_notes']
    # save_as = True
    save_on_top = True
    view_on_site = False  # THIS DOES NOT WORK CURRENTLY
    inlines = (InteractionInline, InteractionNewInline,)

    class Media:
        css = {
            'all': ('css/admin/change-link.css',)
        }

    def email_address_semiprivate_display(self, obj):
        display_text = 'Please contact <strong>{}</strong> for this information'.format(obj.created_by)
        return format_html(display_text)
    email_address_semiprivate_display.short_description = 'Email address'

    def phone_number_primary_semiprivate_display(self, obj):
        display_text = 'Please contact <strong>{}</strong> for this information'.format(obj.created_by)
        return format_html(display_text)
    phone_number_primary_semiprivate_display.short_description = 'Phone number primary'

    def phone_number_secondary_semiprivate_display(self, obj):
        display_text = 'Please contact <strong>{}</strong> for this information'.format(obj.created_by)
        return format_html(display_text)
    phone_number_secondary_semiprivate_display.short_description = 'Phone number secondary'

    def get_expertise(self, obj):
        """ Take any expertise names and create a comma-separated list """
        expertise_name_list = obj.expertise.all().values_list('name', flat=True)
        return ', '.join(expertise_name_list) if expertise_name_list else '-'
    get_expertise.short_description = 'Expertise'

    def get_organizations(self, obj):
        """ Take any org names and create a comma-separated list """
        organization_name_list = obj.organization.all().values_list('name', flat=True)
        return ', '.join(organization_name_list) if organization_name_list else '-'
    get_organizations.short_description = 'Organization(s)'

    def _get_correct_contact_field_names(self, hide_contact_data=False):
        """
        Returns a list of the contact field names that differ depending on privacy. (the base field
        names such as twitter or linkedin are not returned from here)
        """

        prepend_contact_fields = [
            'email_address',
            'phone_number_primary',
            'phone_number_secondary',
        ]

        if hide_contact_data:
            # set the semiprivate fields to the display value
            return [name + '_semiprivate_display' for name in prepend_contact_fields]
        else:
            return prepend_contact_fields

    def _determine_whether_to_hide_contact_data(self, request, obj):
        """
        Given a request and an object, determine if we need to hide the contact data for the object. If
        the object is None (doesn't exist yet), always return False (we don't need to hide anything).

        Returns True if we need to hide the data, False if the user has permissions to see/edit the data.

        NOTE: this could be abstracted out, as it is virtually identical to _determine_whether_to_hide_notes
        in InteractionAdmin.
        """
        if not obj:
            # If creating a new `Person`, give all permissions
            return False
        elif obj.created_by == request.user:
            # Users can always see their own `Person`s
            return False
        elif obj.privacy_level in ['searchable', 'private_individual']:
            # If the `Person` is at all private, and the user didn't create the
            # object, don't allow viewing of contact data
            return True
        else:
            # Only reach here for public `Person`s created by a different user
            return False

    def _return_fieldsets(self, hide_contact_data=False):
        """
        Returns the correct fieldsets based on whether we're hiding data. This is called when we're
        building the fieldsets as well as when we are setting all fields to readonly.

        NOTE: This could be considerably more abstract and thus complicated: it could change fieldsets
        besides the contact info. I decided to shy away from that right now -- we can always come
        back and change it.

        Arguments:
            hide_contact_data - if True, replace the email & phone fields with the semiprivate display values, as well
                as setting the readonly fields to include these fields.
        """

        base_contact_fields = [
            'linkedin',
            'twitter',
            'skype',
        ]
        custom_contact_fields = self._get_correct_contact_field_names(hide_contact_data=hide_contact_data)

        key_info_fields = tuple(
            ['privacy_level', 'name'] +
            custom_contact_fields +
            ['title', 'organization']
        )

        return (
            ('Key info', {
                'fields': key_info_fields
            }),
            ('General info', {
                'classes': ('collapse',),
                'fields': (
                    'prefix',
                    'pronouns',
                    'industries',
                    'website',
                    'type_of_expert',
                    'expertise',
                    'gatekeeper',
                ),
            }),
            ('Additional contact info', {
                'classes': ('collapse',),
                'fields': base_contact_fields
            }),
            ('Location info', {
                'classes': ('collapse',),
                'fields': (
                    'timezone',
                    'city',
                    'state',
                    'province',
                    'country',
                ),
            }),
            ('Export info', {
                'classes': ('collapse',),
                'fields': (
                    'exportable_by_dive',
                    'exportable_by_user',
                ),
            }),
            ('Advanced info', {
                'classes': ('collapse',),
                'fields': (
                    'entry_method',
                    'entry_type',
                    'get_created_by',
                    # 'last_updated_by',
                    'updated',
                    'import_notes',
                ),
            }),
        )

    def get_queryset(self, request):
        """ only show private sources to the person who created them """
        qs = super(PersonAdmin, self).get_queryset(request)

        # if the source is not private, then include them
        # if the source is private and created by that user, then include them
        return qs.filter(
            # IMPORANT! don't give superusers access to everything
            ~Q(privacy_level__contains='private') |
            Q(created_by=request.user, privacy_level='private_individual')
        )

    def get_fieldsets(self, request, obj=None):
        """
        Use Django's built in hook for accessing the fieldsets. Manipulating self.fieldsets directly
        leads to problems.
        """

        # TODO: we may able to delete this if statement, as we believe the first case is covered
        # by the queryset restrictions
        if obj and obj.created_by != request.user and obj.privacy_level == 'private_individual':
            # Cover the one weird edge case not covered by `_determine_whether_to_hide_contact_data`
            # This is if the user is trying to view a person they're not even allowed to know exists
            return [(None, {'fields': []})]
        else:
            # Construct the correct fieldsets based on permissions
            hide_contact_data = self._determine_whether_to_hide_contact_data(request, obj)
            return self._return_fieldsets(hide_contact_data=hide_contact_data)

    def get_readonly_fields(self, request, obj=None):
        """
        Use Django's built in hook for accessing readonly fields. Manipulating self.readonly_fields
        directly leads to problems.
        """
        if not obj:
            # If creating the obj, use only default readonly fields
            return self.readonly_fields

        hide_contact_data = self._determine_whether_to_hide_contact_data(request, obj)

        view_mode = 'edit' not in request.GET
        public_not_creator = obj.created_by != request.user and obj.privacy_level == 'public'

        if view_mode or hide_contact_data:
            # If we're not editing or a semi-private source is being viewed by
            # someone who did not create it, then all fields should be readonly
            # Use _return_fieldsets to get the correct fields for this permission level
            current_fieldsets = self._return_fieldsets(hide_contact_data=hide_contact_data)
            return flatten_fieldsets(current_fieldsets)
        elif public_not_creator:
            # If a source is public and current user is not the creator,
            # then we want to make some additional fields readonly.
            return self.readonly_fields + ['privacy_level', 'exportable_by_dive', 'exportable_by_user']
        else:
            # If we're editing AND the user has permissions, use the default fields
            return self.readonly_fields

    def response_change(self, request, obj):
        url = reverse('admin:sources_person_change', args=(obj.id,))

        if '_edit' in request.POST:
            return HttpResponseRedirect(url + '?edit=true')
        elif '_view' in request.POST:
            return HttpResponseRedirect(url)

        return super(PersonAdmin, self).response_change(request, obj)

    def save_model(self, request, obj, form, change):
        # associate the Person being created with the User who created them
        current_user = request.user
        if not obj.created_by:
            obj.created_by = current_user
        if not obj.entry_method:
            obj.entry_method = 'admin-form'

        super(PersonAdmin, self).save_model(request, obj, form, change)


class ArticleMentionsWidget(forms.Widget):
    template_name = 'admin/article_mentions_widget.html'

    def get_context(self, name, value, attrs=None):
        article_mentions = json.loads(value)
        return {'widget': {
            'article_mentions': tuple(article_mentions.items()),
        }}

    def render(self, name, value, attrs=None, renderer=None):
        context = self.get_context(name, value, attrs)
        template = loader.get_template(self.template_name).render(context)
        return mark_safe(template)


class NameAdmin(admin.ModelAdmin):
    list_display = ['name', 'not_a_person']
    list_editable = ['not_a_person']
    fields = [
        # need to list out the fields because readonly_fields reorders them
        # by pushing the non-editable ones to the bottom
        'name',
        'title',
        'gender_guess',
        'not_a_person',
        'article_mentions'
    ]
    readonly_fields = [
        'name',
        'title',
        'gender_guess',
    ]

    formfield_overrides = {
        JSONField: {'widget': ArticleMentionsWidget}
    }


admin.site.register(Dive, DiveAdmin)
admin.site.register(Expertise, ExpertiseAdmin)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(Industry, IndustryAdmin)
admin.site.register(Interaction, InteractionAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(Name, NameAdmin)

admin.site.site_header = 'Source Dive'
