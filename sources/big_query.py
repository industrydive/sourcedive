import os

from django.conf import settings

from google.cloud import bigquery


class BigQueryService:
    """
    A wrapper around the big query client that allows for some convenience methods. Requires a big query project for initialization.
    """
    def __init__(self):
        self.project = settings.GOOGLE_BIG_QUERY_PROJECT
        self.google_application_credentials = os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = settings.GOOGLE_BIG_QUERY_API_KEY_FILE_LOCATION
        self.client = bigquery.Client(project=self.project)

    def query_table(self, query):
        """
        Query a table

        :param query: the query to run
        :type query: str
        :return: the query result
        :rtype: bigquery.QueryJob
        """
        bq_query_job = self.client.query(query)
        return bq_query_job.result()

    def query_newsposts(self):
        """
        Get yesterday's newsposts
        """
        newspost_query = """
        SELECT id, url, body
        FROM divesite.newsposts_latest
        WHERE body IS NOT NULL AND
        published IS TRUE AND
        DATE(pub_date) = CURRENT_DATE() - 1
        """

        newsposts = self.query_table(newspost_query)

        return newsposts
