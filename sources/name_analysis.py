from bs4 import BeautifulSoup

import gender_guesser.detector as gender
gender_guesser = gender.Detector()
from find_job_titles import Finder
finder = Finder()
import spacy
nlp = spacy.load("en_core_web_sm")

from sources.models import Name


class GetNames:
    """
    Get mentioned names in newsposts. This runs nightly and looks over the
    newsposts from the previous day.
    """
    def __init__(self, newspost):
        newspost_body = self.get_newspost_text(newspost['body'])
        self.name_results = nlp(newspost_body)  # get names from Spacy reading of newspost body
        self.current_newspost = newspost['url']
        self.newspost_full_names = {}  # keep track of full names mentioned
        self.newspost_last_name_to_full_name = {}  # map each last name to first name

        for spacy_name_obj in self.name_results.ents:  # loop over Spacy name objects
            if spacy_name_obj.label_ == 'PERSON':  # if name is a person
                self.name_text = spacy_name_obj.text.strip()
                self.spacy_name_obj = spacy_name_obj
                if self.is_valid_name():
                    self.record_name()

    def get_newspost_text(self, body):
        """ Use BeautifulSoup to get text from newspost body """
        soup = BeautifulSoup(body, 'html.parser')
        # remove image captions from the newspost body because of funky name formatting
        for div in soup.find_all("figcaption", {'class': 'inside_story_caption'}):
            div.decompose()
        return soup.get_text()

    def record_name(self):
        """ Record name and info """
        if self.get_or_create_name():
            self.add_mention()
            self.record_title()
            self.record_gender()
            self.name.save()

    def get_or_create_name(self):
        """ Get existing name in db, new name, or mentioned name """
        if self.is_full_name():
            # if is a full name, get existing name in db or create a new name
            name = self.existing_name() or self.new_name()
        else:
            # if is last name, get already mentioned full name if present
            name = self.mentioned_name()

        if name:
            self.name = name
            return True

    def existing_name(self):
        """ Get the name if it already exists"""
        return Name.objects.filter(name=self.name_text).first()

    def new_name(self):
        """ Create a new name """
        return Name.objects.create(name=self.name_text)  # make a new name

    def mentioned_name(self):
        """ Get the full name if the current name is just a last name and was already mentioned in the newspost """
        full_name = self.newspost_last_name_to_full_name.get(self.name_text, None)
        if full_name:
            return Name.objects.get(name=full_name)

    def add_mention(self):
        """ Add 1 to the name's mention amount for the article. If it is the first mention of a name,
        set/reset the mention amount to 0 - this is just in case an article is analyzed more than once,
        so we don't recount the mentions for a name. Also track the person's full name and last name """
        if self.is_first_mention():
            self.name.article_mentions[self.current_newspost] = 0
            self.newspost_full_names[self.name_text] = True
            self.track_last_name()

        self.name.article_mentions[self.current_newspost] += 1

    def is_first_mention(self):
        """ Name counts as first mention if it is a full name and hasn't been mentioned in the article yet """
        if self.is_full_name() and self.name_text not in self.newspost_full_names:
            return True
        else:
            return False

    def record_title(self):
        """ Get the job title if present in the words preceding the name """
        prev_words = [self.name_results[self.spacy_name_obj.start - i].text for i in [3, 2, 1]]  # look at previous 3 words
        prev_words_as_text = ' '.join(prev_words)
        try:
            title_results = finder.findall(prev_words_as_text)[0]
            title = title_results[2]
            self.name.title = title
        except:  # noqa: E722
            return  # title not present

    def record_gender(self):
        """ Guess the gender of the name """
        first_name = self.name_text.split(' ')[0]
        gender_guess = gender_guesser.get_gender(first_name)
        self.name.gender_guess = gender_guess

    def is_full_name(self):
        """ Counts as a full name if there is a space in the name """
        return ' ' in self.name_text

    def is_valid_name(self):
        """ A little extra validation to not count names with invalid characters or that end with 's """
        invalid_characters = '<>="&;:[]%0'
        name_has_valid_characters = len(set(self.name_text).intersection(invalid_characters)) == 0

        name_does_not_end_with_apostrophe_s = self.name_text.replace("’", "'")[-2: len(self.name_text)] != "'s"

        return name_has_valid_characters and name_does_not_end_with_apostrophe_s

    def track_last_name(self):
        """ If the name is a full name, track the last name so we know if it is mentioned later in the article """
        last_name = self.name_text.split(' ')[-1]
        self.newspost_last_name_to_full_name[last_name] = self.name_text
