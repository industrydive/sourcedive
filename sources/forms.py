from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple

from sources.models import Person


class SourceCreationSimpleForm(forms.ModelForm):
    class Media:
        js = (
            '/admin/jsi18n/',
            'admin/js/jquery.init.js',
        )
        css = {
            'all': (
                'admin/css/base.css',
                'admin/css/forms.css',
            )
        }

    class Meta:
        model = Person
        fields = (
            'privacy_level',
            'name',
            'email_address',
            'phone_number_primary',
            'title',
            'organization',
            'created_by',
        )
        widgets = {
            'organization': FilteredSelectMultiple('Organizations', is_stacked=False),
        }
