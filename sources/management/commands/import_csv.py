import csv
from datetime import datetime
import sys

from django.apps import apps
from django.core.management.base import BaseCommand

from sources.models import Person

csv_name_to_model_name = {
    'expertise': ('Expertise', 'expertise'),
    'industries': ('Industry', 'industries'),
    'organization': ('Organization', 'organization'),
    'exportable_by_dive': ('Dive', 'exportable_by_dive'),
    'exportable_by_user': ('User', 'exportable_by_user'),
}


def _get_model_by_name(app_label, model_name):
    return apps.get_model(app_label=app_label, model_name=model_name)


def _get_or_create_object(obj_values, app_label, model_name):
    model = _get_model_by_name(app_label, model_name)
    obj, obj_created = model.objects.get_or_create(**obj_values)
    return obj, obj_created


def _add_values_to_source(field, values_list, person_obj, app_label, model_name):
    model = _get_model_by_name(app_label, model_name)
    for value in values_list:
        obj, obj_created = model.objects.get_or_create(**value)
        getattr(person_obj, field).add(obj)


def _create_dictionary_for_email_data(emails_as_string):
    return [
        {
            'email': f'{value.strip()}@industrydive.com',
            'username': value.strip(),
        } for value in emails_as_string.split(',')
    ]


def _add_m2m_fields_to_source(person_obj, m2m_dict):
    for m2m_key in m2m_dict.keys():
        if m2m_key == 'created_by':
            print('Adding the created by user to the source')
            created_by_email = m2m_dict['created_by']
            created_by_email = {'email': created_by_email, 'username': created_by_email.split('@')[0]}

            user, user_created = _get_or_create_object(created_by_email, 'auth', 'User')
            person_obj.created_by = user
            person_obj.save()

            print('Successfully added the user to source')
        if m2m_key in ['expertise', 'industries', 'organization', 'exportable_by_dive', 'exportable_by_user']:
            print(f'Adding {m2m_key} to the source')

            if m2m_key == 'exportable_by_user':
                values_list = _create_dictionary_for_email_data(m2m_dict[m2m_key])
                app_label = 'auth'
            else:
                values_list = [{'name': value.strip()} for value in m2m_dict[m2m_key].split(',')]
                app_label = 'sources'

            model_name, field_name = csv_name_to_model_name[m2m_key]
            _add_values_to_source(
                field=field_name,
                values_list=values_list,
                person_obj=person_obj,
                app_label=app_label,
                model_name=model_name,
            )
            print(f'Successfully added {m2m_key} to the source')


def create_person(data_dict, m2m_dict=None):
    """
    Create a Person in the system as part of the import process. Works for
    both import file types.
    """
    email_address = data_dict['email_address']
    # check if the person already exists and skip if exists
    if Person.objects.filter(email_address=email_address).count():
        print(f'Skipping: Person with {email_address} already exists.')
        return

    person_obj, person_created = Person.objects.get_or_create(email_address=data_dict['email_address'], defaults=data_dict)
    print(f'Success: {person_obj}' if person_created else f'Email already existed. Updated: {person_obj}')

    # populate MANY-TO-MANY fields
    if m2m_dict:
        try:
            _add_m2m_fields_to_source(person_obj, m2m_dict)
        except Exception as e:
            create_message = f'Error for {email_address}: {e}\n'


def import_csv(csv_file):
    ## start
    start_time = datetime.now()
    start_message = '\nStarted import:\t {}\n'.format(start_time)
    print(start_message)

    # row_count = sum(1 for row in csv_reader)
    # message = 'Number of rows: {}\t'.format(row_count)
    # print(message)
    counter = 0
    failed_rows = 0

    # TODO: make this less hacky/kludgy and improve error handling + reporting
    if 'latest_export.csv' in csv_file:
        with open(csv_file, 'r') as file:
            reader = csv.DictReader(file)
            for row in reader:
                counter += 1
                row_as_dict = dict(row)
                now = str(timezone.now())
                # we never want to use the old related user id bc a new one needs to be made
                row_as_dict.pop('related_user')
                # adjust the following as needed before import
                if row_as_dict['created'] == '':
                    row_as_dict['created'] = now
                if row_as_dict['updated'] == '':
                    row_as_dict['updated'] = now
                if row_as_dict['timezone'] == '':
                    row_as_dict.pop('timezone')
                if row_as_dict['rating'] == '':
                    row_as_dict.pop('rating')
                if row_as_dict['rating_avg'] == '':
                    row_as_dict.pop('rating_avg')
                try:
                    create_person(row_as_dict)
                except:
                    email_address = row_as_dict['email_address']
                    message = 'Failed to create a person for {}. \nException: {}'.format(
                            email_address,
                            str(sys.exc_info())
                        )
                    print(message)
    else:
        with open(csv_file) as file:
            csv_reader = csv.DictReader(file)
            ## loops thru the rows
            for counter, row in enumerate(csv_reader):
                ## special fields
                status = 'added_by_admin'
                email_address = row['email_address']
                timezone =row['timezone']
                if isinstance(timezone, int):
                    timezone_value = timezone
                else:
                    timezone_value = None

                ## map fields from csv to Person model
                csv_to_model_dict = {
                    # 'role': row['role'],
                    'privacy_level': row['privacy_level'],
                    'name': row['name'],
                    'type_of_expert': row['type_of_expert'],
                    'title': row['title'],
                    'city': row['city'],
                    'state': row['state'],
                    'province': row['province'],
                    'country': row['country'],
                    'phone_number_primary': row['phone_number_primary'],
                    'phone_number_secondary': row['phone_number_secondary'],
                    'twitter': row['twitter'],
                    'import_notes': row['import_notes'],
                    'website': row['website'],
                    'prefix': row['prefix'],
                    'entry_method': 'import',
                    'entry_type': 'automated',
                    'email_address': email_address,
                    # 'status': status,
                    'timezone': timezone_value,
                    'gatekeeper': row['gatekeeper'],
                    'updated': row['updated']
                }
                m2m_dict = {
                    'created_by': row['created_by'],
                    'expertise': row['expertise'],
                    'industries': row['industries'],
                    'organization': row['organization'],
                    'exportable_by_dive': row['exportable_by_dive'],
                    'exportable_by_user': row['exportable_by_user'],
                }
                create_person(csv_to_model_dict, m2m_dict)
        # message = '\nThe following rows failed: \n\n {}'.format(failed_rows)
        # print(message)

    ## end
    end_time = datetime.now()
    end_message = '\nFinished import:\t {} \n'.format(end_time)
    import_length = end_time - start_time
    message = end_message
    print(message)
    message = 'Import length:\t\t {} \n'.format(import_length)
    print(message)

    # message = 'Imported {} rows'.format(counter)
    # print(message)
    # message = '{} rows failed'.format(failed_rows)
    # print(message)


class Command(BaseCommand):
    help = 'Import sources from a csv file.'

    def add_arguments(self, parser):
        ## required
        parser.add_argument('file', 
            help='Specify the CSV file.'
        )

    def handle(self, *args, **options):
        ## unpack args
        csv_file = options['file']

        ## call the function
        import_csv(csv_file)

