from django.core.management.base import BaseCommand, CommandError

from sources.big_query import BigQueryService
from sources.name_analysis import GetNames


class Command(BaseCommand):
    help = "Imports the names mentioned in yesterday's published articles. Runs daily."

    def handle(self, *args, **options):
        newsposts = BigQueryService().query_newsposts()
        
        for newspost in newsposts:
            GetNames(newspost)
