from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

from sources.views import bookmarklet_form, bookmarklet_add_source, download_sources, analyze_names


urlpatterns = [
    # sourcedive
    url(r'^admin/download', download_sources, name='download_sources'),
    url(r'^admin/bookmarklet/$', bookmarklet_form, name='bookmarklet'),
    url(r'^admin/bookmarklet/add/$', bookmarklet_add_source, name='bookmarklet_add'),
    url(
        r'^admin/bookmarklet/setup/$',
        TemplateView.as_view(template_name='admin/sources/bookmarklet/setup.html'),
        name='bookmarklet_setup'
    ),
    url(r'^admin/analyze_names', analyze_names, name='analyze_names'),
    # general
    url(r'^admin/', admin.site.urls),
    # social auth
    url(r'^accounts/login/$', auth_views.LoginView.as_view()),
    # url('', include('social.apps.django_app.urls', namespace='social')),
    url('', include(('social_django.urls', 'social_django'), namespace='social')),
    url('', include(('django.contrib.auth.urls', 'django'), namespace='auth')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
