"""
Django settings for sourcedive project.
"""

import os

TEST_ENV = os.getenv('TEST_ENV') == 'True'
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.getenv('SOCIAL_AUTH_GOOGLE_OAUTH2_KEY')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.getenv('SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET')

SOCIAL_AUTH_PASSWORDLESS = os.getenv('SOCIAL_AUTH_PASSWORDLESS') == 'True'
SOCIAL_AUTH_ALWAYS_ASSOCIATE = os.getenv('SOCIAL_AUTH_ALWAYS_ASSOCIATE') == 'True'

db_engine = os.getenv('DATABASE_ENGINE')
db_name = os.getenv('DATABASE_NAME')
db_host = os.getenv('DATABASE_HOST')
db_port = os.getenv('DATABASE_PORT')

db_user = os.getenv('DATABASE_USER')
db_password = os.getenv('DATABASE_PASSWORD')

SECRET_KEY = os.getenv('SECRET_KEY')

INTERNAL_IPS = os.getenv('INTERNAL_IPS').split(',')

# LOAD_RUNNER_IP should be the google kuberenetes cluster load runner ip
ALLOWED_HOSTS = [
    'sources.dive.tools',
    '127.0.0.1',
    'localhost'
]

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

# SECURITY WARNING: don't run with debug turned on in production!
if TEST_ENV:
    DEBUG = True
else:
    DEBUG = False

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'sources.apps.SourcesConfig',
    'social_django',
    'debug_toolbar',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # Locale must go after Session and before Common
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

ROOT_URLCONF = 'sourcedive.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
                'sources.context_processors.sources_context_processor'
            ],
        },
    },
]

WSGI_APPLICATION = 'sourcedive.wsgi.application'


# Database

DATABASES = {
    'default': {
        'ENGINE': db_engine,
        'NAME': db_name,
        'USER': db_user,
        'PASSWORD': db_password,
        'HOST': db_host,
        'PORT': db_port,
    }
}


# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/New_York'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, 'media'),
)

# Python social auth
AUTHENTICATION_BACKENDS = (
    'social_core.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_URL_NAMESPACE = 'social'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/admin/sources/person/'
# Recommended when using Postgres. See https://python-social-auth.readthedocs.io/en/latest/configuration/django.html#database.
SOCIAL_AUTH_JSONFIELD_ENABLED = True

# explanation and other options: https://python-social-auth-docs.readthedocs.io/en/latest/pipeline.html

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

LOCALE_PATHS = [os.path.join(BASE_DIR, 'locale/')]

# Default BQ project to floaties so we don't accidentally update data in production (divedatamart)
# from local or QA environments. This is overwritted in prod local_settings.py
GOOGLE_BIG_QUERY_PROJECT = 'divedatamart'

# TECH-7492: Provide authorization credentials for API access to BigQuery Fishinghole project
GOOGLE_BIG_QUERY_API_KEY_FILE_LOCATION = 'google_big_query_settings.json'
