#!/bin/bash
# add needed env variables to run management command
export SECRET_KEY=krYH3%WK!65PL!g9
export PATH=/venv/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export INTERNAL_IPS=127.0.0.1
export DATABASE_USER=sourcediveuser
export DATABASE_ENGINE=django.db.backends.postgresql_psycopg2
export DATABASE_NAME=sourcedive
export DATABASE_PASSWORD=sourcediveuser
export DATABASE_HOST=db

python manage.py import_names
